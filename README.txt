CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module adds a Commerce popup cart block. It will display a cart icon with 
a dynamic number representing the number of items in the cart. 
When hovered over the cart will popup with its items listings and action links.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_cart_popup

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_cart_popup

REQUIREMENTS
------------

This module requires the following modules:

 * Commerce (https://www.drupal.org/project/commerce)
 

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no configuration settings. 

MAINTAINERS
-----------

Current maintainers:
 * Ashutosh Mishra (ashutosh.mishra) - https://www.drupal.org/user/3565900
